package org.runite.client;
import java.awt.Component;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.Mixer.Info;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.*;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;


final class JavaAudioChannel extends AudioChannel {

   private int sBufferSize;
   private SourceDataLine speakers;
   private boolean isSoundMax = false;
   private AudioFormat audioFormat;
   private byte[] speakerByteArray;
   boolean playing = false;
   int sourceId;
   ByteBuffer pcm = BufferUtils.createByteBuffer(512); // about 1/4 second at 44.1khz

   final void close() {
      if (null != this.speakers) {
         this.speakers.close();
         this.speakers = null;
      }

   }

   final void init(Component var1) {
      Info[] mixerInfo = AudioSystem.getMixerInfo();
      if (null != mixerInfo) {
         for (Info mixerAttribute : mixerInfo) {
            if (null != mixerAttribute) {
               String mixerName = mixerAttribute.getName();
               if (null != mixerName && mixerName.toLowerCase().contains("soundmax")) {
                  this.isSoundMax = true;
               }
            }
         }
      }

      this.audioFormat = new AudioFormat((float) Class21.sampleRate, 16, !AudioChannel.stereo ? 1 : 2, true, false);
      this.speakerByteArray = new byte[256 << (AudioChannel.stereo ? 2 : 1)];

   }

   final void open(int bufferSize) throws LineUnavailableException {
      try {
         javax.sound.sampled.DataLine.Info dataLine = new javax.sound.sampled.DataLine.Info(
                 SourceDataLine.class,
                 this.audioFormat,
                 bufferSize << (!AudioChannel.stereo ? 1 : 2)
         );
         this.speakers = (SourceDataLine) AudioSystem.getLine(dataLine);
         this.speakers.open();
         this.speakers.start();
         this.sBufferSize = bufferSize;
      } catch (LineUnavailableException lineError) {
         if (Class146.method2080(bufferSize) == 1) {
            this.speakers = null;
            throw lineError;
         } else {
            this.open(Class95.method1585((byte) 76, bufferSize));
         }
      }
      // Init OpenAL
      String defaultDevice = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);
      long audioDevice = alcOpenDevice(defaultDevice);
      int[] attributes = {0};
      long audioContext = alcCreateContext(audioDevice, attributes);
      alcMakeContextCurrent(audioContext);

      ALCCapabilities alcCapabilities = ALC.createCapabilities(audioDevice);
      ALCapabilities alCapabilities = AL.createCapabilities(alcCapabilities);

      if (alCapabilities.OpenAL10) {
         System.out.println("OPENAL SUPPORTED!");
      } else {
         System.out.println("ERROR: OPENAL NOT SUPPORTED!");
      }
      this.sourceId = alGenSources();
   }

   final void flush() throws LineUnavailableException {
      this.speakers.flush();
      if (this.isSoundMax) {
         this.speakers.close();
         this.speakers = null;
         // Open a new dataline
         javax.sound.sampled.DataLine.Info dataLine = new javax.sound.sampled.DataLine.Info(
                 SourceDataLine.class,
                 this.audioFormat,
                 this.sBufferSize << (!AudioChannel.stereo ? 1 : 2)
         );
         this.speakers = (SourceDataLine) AudioSystem.getLine(dataLine);
         this.speakers.open();
         this.speakers.start();
      }
   }

   final int getRemainingSamples() {
      return this.sBufferSize - (this.speakers.available() >> (!AudioChannel.stereo ? 1 : 2));
   }


   // Music Frequency 22050
   // SFX Frequency 2048
   final void write() {
      int bufferSize = 256;
      int format = AL_FORMAT_MONO16;
      if (AudioChannel.stereo) {
         bufferSize <<= 1;
         format = AL_FORMAT_STEREO16;
      }

      // Convert pcm signed floats to bytes
      // https://stackoverflow.com/questions/50672068/java-audio-sourcedataline-does-not-support-pcm-float
      for (int i = 0; i < bufferSize; ++i) {
         int bufferSample = this.samples[i];
         if ((bufferSample + 8388608 & -16777216) != 0) {
            bufferSample = 8388607 ^ bufferSample >> 31;
         }
         this.speakerByteArray[i * 2] = (byte) (bufferSample >> 8);
         this.speakerByteArray[i * 2 + 1] = (byte) (bufferSample >> 16);
         pcm.put(i, (byte) ((bufferSample >> 16)*2));
      }

      if (!this.playing || (alGetSourcei(this.sourceId, AL_SOURCE_STATE) == AL_STOPPED)) {
         this.playing = true;
         int b = alGenBuffers();
         alBufferData(b, format, this.pcm, 22050);
         AL10.alSourceQueueBuffers(this.sourceId, b);
         System.out.println("Playing sound ...");
         alSourcePlay(this.sourceId);
      }

      int b = alSourceUnqueueBuffers(this.sourceId);
      alBufferData(b, format, this.pcm, 22050);
      alSourceQueueBuffers(this.sourceId, b);
      alSourcePlay(this.sourceId);

      // just to show if we stop filling buffers it stops!
      if (alGetSourcei(this.sourceId, AL_SOURCE_STATE) == AL_STOPPED) {
         System.out.println("source stopped ");
         this.playing = false;
      }
      this.speakers.write(this.speakerByteArray, 0, bufferSize << 1);
   }
}